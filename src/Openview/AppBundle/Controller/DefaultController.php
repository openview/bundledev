<?php

namespace Openview\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OpenviewAppBundle:Default:index.html.twig');
    }
}
