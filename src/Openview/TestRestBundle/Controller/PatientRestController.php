<?php
namespace Openview\TestRestBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Openview\TestRestBundle\Entity\Patient;
use Openview\TestRestBundle\Form\Type\PatientType;

/**
 * Basato su http://williamdurand.fr/2012/08/02/rest-apis-with-symfony2-the-right-way/
 */
class PatientRestController extends FOSRestController
{
    
    /**
     * Ritorna tutti gli elementi
     * 
     * @return array
     * @Rest\View()
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('OpenviewTestRestBundle:Patient')->findAll();
        
        return array(
            'entities' => $entities,
        );
    }
    
    
    /**
     * Get action
     * 
     * @var integer $id Id of the entity
     * @return array
     *
     * @Rest\View()
     */
    public function getAction($id)
    {
        $entity = $this->getEntity($id);

        return array(
            'entity' => $entity,
        );
    }
    
    
    
    /**
     * Create a new item
     * 
     * Se invio i dati via json, devo usare la forma:
     * curl -v -H "Accept: application/json" -H "Content-type: application/json" 
     *  -X POST -d '{"patient":{"name":"foo", "age": "41", "password": "secret"}}' http://.../app_dev.php/api/v1/patient/new.json
     * 
     * @return type
     */
    public function newAction()
    {
        return $this->processForm(new Patient());
    }
    
    
    
    
    /**
     * Get entity instance
     * 
     * @var integer $id Id of the entity
     * @return Patient
     */
    protected function getEntity($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('OpenviewTestRestBundle:Patient')->find($id);
        if (!($entity instanceof Patient)) {
            throw $this->createNotFoundHttpException('Unable to find entity');
        }

        return $entity;
    }
    
    
    
    /**
     * Elabora un form
     * 
     * @param Patient $item
     * @return Response
     * 
     * @Rest\View()
     */
    private function processForm(Patient $item)
    {
        $em = $this->getDoctrine()->getManager();
        $statusCode = ($item->getId() === null) ? 201 : 204;
        //echo $statusCode; exit;
        $form = $this->createForm(new PatientType(), $item);
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            $em->persist($item);
            $em->flush();
            $response = new Response();
            $response->setStatusCode($statusCode);
            // set the `Location` header only when creating new resources
            if (201 === $statusCode) {
                $response->headers->set('Location',
                    $this->generateUrl(
                        'api_v1_patient_get', array('id' => $item->getId()),
                        true // absolute
                    )
                );
            }

            return $response;
        }

        return View::create($form, 400);
    }

    
}