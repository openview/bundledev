<?php
namespace Openview\TestRestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Patient example
 *
 * @ORM\Entity
 * @ORM\Table(name="patient")
 * @ExclusionPolicy("all")
 */
class Patient {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     * @Expose
     * @Assert\NotBlank()
     */
    protected $name;
    /**
     * @ORM\Column(type="integer")
     * @Expose
     * @Assert\Range(
     *      min = 18,
     *      max = 250,
     *      minMessage = "You seems too young",
     *      maxMessage = "We don't think you are {{ limit }}cm tall"
     * )
     */
    protected $age;
    /**
     * password. Non viene mostrata nella API REST, vedere configurazione del serializzatore
     * @ORM\Column(type="string")
     *  @Assert\Length(
     *      min = 8,
     *      max = 32,
     *      minMessage = "Minimum length: {{ limit }} characters",
     *      maxMessage = "Maximum length: {{ limit }} characters"
     * )
     */
    protected $password;
    
    
    
    public function __construct($name=null, $age=null) {
        $this->id = null;
        $this->name = $name;
        $this->age = $age;
    }
    
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getAge() {
        return $this->age;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setAge($age) {
        $this->age = $age;
    }
    
    function getPassword() {
        return $this->password;
    }

    function setPassword($password) {
        $this->password = $password;
    }




}
