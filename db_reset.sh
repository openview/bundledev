#!/bin/bash

if [[ ! -e "app/console" ]]
then
	echo "\nYou need to be in the root directory of the Symfony project to run this script!\n"
	exit
fi

kamikaze=0
mute=0
dirty=0
orm=1
odm=1
fixtures=1

while [ "$1" != "" ]; do
    case $1 in
        -k | --kamikaze )       kamikaze=1
                                ;;
        -m | --mute ) 					mute=1
                                ;;
        -d | --dirty )          dirty=1
                                ;;  
        --no-odm )							odm=0
        												;;
        --no-orm )							orm=0
        												;;
        --no-fixtures	) 				fixtures=0
        												;;
        * )                     echo "Unknown option $1"
        												exit 1
    esac
    shift
done


if [[ $kamikaze != 1 ]]
then
	echo "\nI am going to erease all the content of the databases!\n"
	echo "Continue? (y/n): ";
	read cont

	if [[ ($cont != 'Y') && ($cont != 'y') ]]
	then
		echo "Ok. See you next time :)\n"
		exit
	fi
fi

if [[ $orm != 0 ]]
then
	if [[ $mute != 1 ]]
	then
		echo "DROPPING MYSQL DATABASE"
	fi
	php app/console doctrine:database:drop --force
fi

if [[ $odm != 0 ]]
then
	if [[ $mute != 1 ]]
	then
		echo "DROPPING MONGO DATABASE"
	fi
	php app/console doctrine:mongodb:schema:drop
fi

if [[ $orm != 0 ]]
then
	if [[ $mute != 1 ]]
	then
		echo "INIT NEW MYSQL DATABASE"
	fi
	php app/console doctrine:database:create
	php app/console doctrine:schema:update --force
fi

if [[ $odm != 0 ]]
	then
	if [[ $mute != 1 ]]
	then
		echo "INIT NEW MONGODB DATABASE"
	fi
	php app/console doctrine:mongodb:schema:create
fi

if [[ $fixtures != 0 ]]
then
	if [[ $orm != 0 ]]
	then
		if [[ $mute != 1 ]]
		then
			echo "LOADING ENTITY FIXTURES"
		fi
		php app/console doctrine:fixtures:load
	fi	

	if [[ $odm != 0 ]]
	then
		if [[ $mute != 1 ]]
		then
			echo "LOADING DOCUMENT FIXTURES"
		fi
		php app/console doctrine:mongodb:fixtures:load
	fi
fi

if [[ $dirty != 1 ]]
then
    php app/console cache:clear
    php app/console cache:clear --env=test
fi    

if [[ $mute != 1 ]]
then
	echo "\nEnjoy your new databases :)\n"
fi
