#!/bin/sh
php composer.phar update
./db_reset.sh --no-odm
./fix_permission.sh
php app/console cache:clear --no-warmup --env=prod
php app/console assetic:dump --no-debug --env=prod
php app/console assets:install web
